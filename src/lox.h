#ifndef LOX
#define LOX

void lox_report(int line, char* where, char* message);
void lox_error(int line, char* message);

int run_file(char*);
void run_prompt();

#endif // !LOX
