#include <stdio.h>
#include <stdlib.h>
#include "yq_utils.h"

int yq_strlen(const char* str)
{
    int a = 0;
    for (char* p = (char*)str; *p != 0; p++)
        a++;
    return a;
}

void* yq_memcpy(const void* src, void* dest, int size)
{
    char* s = (char*)src;
    char* d = dest;
    while(size--)
        *d++ = *s++;
    return dest;
}

char* yq_strdup(const char* src)
{
    int len = yq_strlen(src);
    char* dest = malloc(len + 1);
    if (dest == NULL)
    {
        return NULL;
    }
    yq_memcpy(src, dest, len + 1);
    return dest;
}

char* yq_strndup(const char* src, int size)
{
    char* dest = malloc(size + 1);
    if (dest == NULL)
        return NULL;
    char* s = (char*)src;
    char* d = dest;
    int i = size;
    while(i--)
    {
        // copy src to dest
        *d++ = *s;
        if (*s != 0)
            s++;
    }
    dest[size] = 0;
    return dest;
}

char* yq_read_entire_file(FILE *stream)
{
    char* filestr = malloc(BUF_SIZE * sizeof(char));
    int idx = 0;
    int curr_len = BUF_SIZE * sizeof(char);
    if (filestr == NULL)
    {
        return NULL;
    }
    int c;
    while((c = fgetc(stream)) != EOF)
    {
        filestr[idx] = (char)c;
        idx++;
        if (idx > curr_len)
        {
            filestr = realloc(filestr, curr_len * 2);
            if (filestr == NULL)
            {
                return NULL;
            }
            curr_len *= 2;
        }
    }
    filestr[idx] = 0;
    return filestr;
}

char* yq_read_line(FILE *stream)
{
    // allocate a buffer
    char* line = malloc(BUF_SIZE * sizeof(char));
    int idx = 0;
    int curr_len = BUF_SIZE * sizeof(char);
    if (line == NULL)
    {
        return NULL;
    }
    int c;
    while((c = fgetc(stream)) != EOF)
    {
        line[idx] = (char)c;
        idx++;
        if (idx > curr_len)
        {
            // grow buffer if needed
            line = realloc(line, curr_len * 2);
            if (line == NULL)
            {
                return NULL;
            }
            curr_len *= 2;
        }
        if ((char)c == '\n')
        {
            break;
        }
    }
    // set null terminator
    line[idx] = 0;
    return line;
}

int yq_is_digit(char c)
{
    if (c >= '0' && c <= '9')
        return 1;
    return 0;
}

int yq_is_alpha(char c)
{
    return (c >= 'a' && c <= 'z') || 
           (c >= 'A' && c >= 'Z') ||
           c == '_';
}

int yq_is_alpha_numeric(char c)
{
    return yq_is_alpha(c) || yq_is_digit(c);
}
