#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include "lox.h"
#include "scanner.h"
#include "yq_utils.h"

void lox_report(int line, char* where, char*message)
{
    printf("[line %d] Error %s: %s\n", line, where, message);
}

void lox_error(int line, char* message)
{
    lox_report(line, "", message);
}

int run(char* source)
{
    struct lox_scanner scanner = {
        .source = source,
        .src_len = yq_strlen(source),
        .start = 0,
        .current = 0,
        .line = 1
    };
    scan_tokens(&scanner);

    struct lox_token* p = scanner.tokens.head;
    if (p == NULL)
    {
        return 1;
    }
    while (p->next != NULL)
    {
        printf("%s\n", p->lexeme);
        p = p->next;
    }
    list_cleanup(&scanner.tokens);
    return scanner.error;
}

int run_file(char* path)
{
    FILE *input = fopen(path, "r");
    if (input == NULL)
    {
        perror("Error Opening File: ");
        return 1;
    }
    char* script = yq_read_entire_file(input);
    printf("%s", script);
    free(script);
    return 0;
}

void run_prompt()
{
   while (1)
   {
       printf("> ");
       char* line = yq_read_line(stdin);
       if (*line == 0)
       {
           free(line);
           break;
       }
       run(line);
       free(line);
   }
}

