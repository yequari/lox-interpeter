#ifndef SCANNER_H
#define SCANNER_H
#include "token.h"

struct lox_token_list
{
    struct lox_token* head;
    struct lox_token* tail;
};

/*
 * Adds a token to the end of the list.
 */
void list_add_token(struct lox_token_list*, struct lox_token*);

void list_remove_token(struct lox_token_list*);

/*
 * Frees all nodes stored in the passed token list,
 * but does not free the list itself
 */
void list_cleanup(struct lox_token_list*);

struct lox_scanner
{
    int error;
    char* source;
    unsigned int src_len;
    struct lox_token_list tokens;
    unsigned int start;
    unsigned int current;
    unsigned int line;
};

void scanner_add_token(struct lox_scanner *scanner, token_type type, void* literal);
struct lox_token_list* scan_tokens(struct lox_scanner *scanner);
void scan_token(struct lox_scanner*);

#endif // !SCANNER_H
