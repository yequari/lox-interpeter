#include <stdio.h>
#include "lox.h"

int main(int argc, char *argv[])
{
    if (argc > 2) 
    {
        printf("Usage: clox [script]");
    }
    else if (argc == 2)
    {
        return run_file(argv[1]);
    }
    else {
        run_prompt();
    }
    return 0;
}
