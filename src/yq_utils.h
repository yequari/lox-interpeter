#ifndef YQ_UTILS
#define YQ_UTILS
#include <stdio.h>
#define BUF_SIZE 1048

/*
 * Gets the length of the passed string.
 */
int yq_strlen(const char*);

/*
 * Returns a pointer to a copy of the src string, allocated with malloc. 
 * Returns NULL on error.
 */
char* yq_strdup(const char* src);

char* yq_strndup(const char* src, int size);

/*
 * Copies size bytes from src to dest
 */
void* yq_memcpy(const void* src, void* dest, int size);

/*
 * Allocates a buffer, reads a line from stream, and copies it into the buffer.
 * Buffer is resized if necessary.
 * Returns NULL on error.
 */
char* yq_read_line(FILE*);

/* 
 * Allocates memory and reads entire file into it. 
 */
char* yq_read_entire_file(FILE*);
int yq_is_digit(char c);
int yq_is_alpha(char c);
int yq_is_alpha_numeric(char c);
#endif
