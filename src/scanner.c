#include "scanner.h"
#include "lox.h"
#include "token.h"
#include "yq_utils.h"
#include <stdlib.h>
#include <stdio.h>

void list_add_token(struct lox_token_list* list, struct lox_token *token)
{
    if (list->head == NULL)
    {
        list->head = token;
        list->tail = list->head;
    }
    list->tail->next = token;
    list->tail = list->tail->next;
}

void list_cleanup(struct lox_token_list* list)
{
    struct lox_token* p = list->head;
    while (p != NULL)
    {
        struct lox_token* tmp = p;
        p = p->next;
        if (tmp->literal != NULL)
            free(tmp->literal);
        if (tmp->lexeme != NULL)
            free(tmp->lexeme);
        free(tmp);
    }
}

int is_at_end(struct lox_scanner *scanner)
{
    return scanner->current >= scanner->src_len;
}

char advance(struct lox_scanner *scanner)
{
    return scanner->source[scanner->current++];
}

int match(struct lox_scanner* scanner, char expected)
{
    if (is_at_end(scanner))
        return 0;
    if (scanner->source[scanner->current] != expected)
        return 0;
    scanner->current++;
    return 1;
}

char peek(struct lox_scanner* scanner)
{
    if (is_at_end(scanner))
        return '\0';
    return scanner->source[scanner->current];
}

char peek_next(struct lox_scanner* scanner)
{
    if (scanner->current + 1 >= scanner->src_len)
        return '\0';
    return scanner->source[scanner->current + 1];
}

/* mallocs a literal */
void string(struct lox_scanner* scanner)
{
    while(peek(scanner) != '"' && !is_at_end(scanner))
    {
        if (peek(scanner) == '\n')
            scanner->line++;
        advance(scanner);
    }

    if (is_at_end(scanner))
    {
        lox_error(scanner->line, "Unterminated String");
        return;
    }

    // consume the closing "
    advance(scanner);

    // get literal value
    int literal_size = (scanner->current - 1) - (scanner->start + 1);
    char* literal = malloc(sizeof(char) * (literal_size + 1));
    if (literal == NULL)
    {
        perror("Malloc failed");
        exit(1);
    }
    yq_memcpy(scanner->source + scanner->start, literal, literal_size);
    // null terminate the string
    literal[literal_size] = 0;
    scanner_add_token(scanner, STRING, literal);
}

void number(struct lox_scanner* scanner)
{
    while (yq_is_digit(peek(scanner)))
        advance(scanner);

    // Look for fractional part
    if (peek(scanner) == '.' && yq_is_digit(peek_next(scanner)))
    {
        advance(scanner);
        while (yq_is_digit(peek(scanner)))
            advance(scanner);
    }

    int literal_size = scanner->current - scanner->start;
    char* literal_str = malloc(sizeof(char) * (literal_size + 1));
    if (literal_str == NULL)
    {
        perror("Malloc failed");
        exit(1);
    }
    yq_memcpy(scanner->source + scanner->start, literal_str, literal_size);
    // null terminator
    literal_str[literal_size] = 0;
    double* literal = malloc(sizeof(double));
    if(literal == NULL)
    {
        perror("malloc failed");
        exit(1);
    }
    *literal = atof(literal_str);
    free(literal_str);
    scanner_add_token(scanner, NUMBER, literal);
}

void identifier(struct lox_scanner* scanner)
{
    while (yq_is_alpha_numeric(peek(scanner)))
        advance(scanner);

    scanner_add_token(scanner, IDENTIFIER, NULL);
}

/*
 * Mallocs a new token and adds it to the scanner's token list.
 */
void scanner_add_token(struct lox_scanner *scanner, enum token_type type, void* literal)
{
    struct lox_token* token = malloc(sizeof(struct lox_token));
    if (token == NULL)
    {
        perror("Could not malloc: ");
    }

    // copy lexeme to new char array
    int size = scanner->current - scanner->start;
    char* lex = malloc(sizeof(char) * size + 1);
    if (lex == NULL)
    {
        perror("Malloc Failed");
        exit(1);
    }
    yq_memcpy(scanner->source + scanner->start, lex, size);
    lex[size] = 0;

    token->type = type;
    token->lexeme = lex;
    token->literal = literal;
    token->line = scanner->line;
    token->next = NULL;
    list_add_token(&scanner->tokens, token);
}

struct lox_token_list* scan_tokens(struct lox_scanner *scanner)
{
    while (!is_at_end(scanner))
    {
        scanner->start = scanner->current;
        scan_token(scanner);
    }
    
    scanner_add_token(scanner, EEOF, NULL);
    return &scanner->tokens;
}


void scan_token(struct lox_scanner *scanner)
{

    char c = advance(scanner);
    switch (c) {
        case '(': scanner_add_token(scanner, LEFT_PAREN, NULL); break;
        case ')': scanner_add_token(scanner, RIGHT_PAREN, NULL); break;
        case '{': scanner_add_token(scanner, LEFT_BRACE, NULL); break;
        case '}': scanner_add_token(scanner, RIGHT_BRACE, NULL); break;
        case ',': scanner_add_token(scanner, COMMA, NULL); break;
        case '.': scanner_add_token(scanner, DOT, NULL); break;
        case '-': scanner_add_token(scanner, MINUS, NULL); break;
        case '+': scanner_add_token(scanner, PLUS, NULL); break;
        case ';': scanner_add_token(scanner, SEMICOLON, NULL); break;
        case '*': scanner_add_token(scanner, STAR, NULL); break;
        case '!':
            if (match(scanner, '='))
                scanner_add_token(scanner, BANG_EQUAL, NULL);
            else
                scanner_add_token(scanner, BANG, NULL);
            break;
        case '=':
            if (match(scanner, '='))
                scanner_add_token(scanner, EQUAL_EQUAL, NULL);
            else
                scanner_add_token(scanner, EQUAL, NULL);
            break;
        case '<':
            if (match(scanner, '='))
                scanner_add_token(scanner, LESS_EQUAL, NULL);
            else
                scanner_add_token(scanner, LESS, NULL);
            break;
        case '>':
            if (match(scanner, '='))
                scanner_add_token(scanner, GREATER_EQUAL, NULL);
            else
                scanner_add_token(scanner, GREATER, NULL);
            break;
        case '/':
            if (match(scanner, '/')) {
                // A comment goes until the end of the line.
                while (peek(scanner) != '\n' && !is_at_end(scanner))
                    advance(scanner);
            } else
                scanner_add_token(scanner, SLASH, NULL);
            break;
        case ' ':
        case '\r':
        case '\t':
            // ignore whitespace
            break;
        case '\n':
            scanner->line++;
            break;
        case '"': string(scanner); break;
        default:
            if (yq_is_digit(c))
                number(scanner);
            else if (yq_is_alpha(c))
                identifier(scanner);
            else
               lox_error(scanner->line, "Unexpected character.");
           break;
    }
}
